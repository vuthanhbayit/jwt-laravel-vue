<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert(
            [
                'name' => 'system_setting',
                'description' => 'System Setting'
            ]
        );
        DB::table('permissions')->insert(
            [
                'name' => 'write_permission',
                'description' => 'Write Permission'
            ]
        );
        DB::table('permissions')->insert(
            [
                'name' => 'read_permission',
                'description' => 'Read Permission'
            ]
        );
    }
}
