<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;

use App\Http\Requests;

class PermissionsController extends Controller
{

    public function index() {
        $permissions = Permission::select(['id', 'name', 'description', 'status'])->get();
        return response()->json([
            'status' => 'success',
            'data' => $permissions,
        ]);
    }
}