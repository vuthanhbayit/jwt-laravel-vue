<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Permission.
 *
 * @package namespace App\Models;
 */
class Permission extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'description', 'status'];
    protected $hidden = ['pivot'];
    public function roles() {
        return $this->belongsToMany('App\Models\Role', 'role_permission');
    }
}
