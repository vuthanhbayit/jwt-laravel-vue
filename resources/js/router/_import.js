function _import_component (path) {
        return require(`../components/${path}.vue`).default
}

function _import_page (path) {
        return require(`../pages/${path}.vue`).default
}
export { _import_component, _import_page };