import VueRouter from 'vue-router'
// Pages
import {_import_page} from './_import'
// Routes
const routes = [
    {
        path: '/',
        component: require('@/AppWrapper').default,
        children: [
            {
                path: '/',
                name: 'home',
                component: _import_page('Home'),
                meta: {
                    auth: undefined
                }
            },
            {
                path: '/about',
                name: 'about',
                component: _import_page('About'),
                meta: {
                    auth: undefined
                }
            },
            // USER ROUTES
            {
                path: '/dashboard',
                name: 'dashboard',
                component: _import_page('user/Dashboard'),
                meta: {
                    auth: true
                }
            },
            // ADMIN ROUTES
            {
                path: '/admin',
                name: 'admin.dashboard',
                component: _import_page('admin/Dashboard'),
                meta: {
                    auth: {
                        roles: {
                            role_id: [1, 2]
                        },
                        redirect: {
                            name: 'login'
                        },
                        forbiddenRedirect: '/403'
                    }
                }
            },
            {
                path: '/permission',
                name: 'permission',
                component: _import_page('permission/index'),
                meta: {
                    auth: {
                        roles: {
                            role_id: [1, 2]
                        },
                        redirect: {
                            name: 'login'
                        },
                        forbiddenRedirect: '/403'
                    }
                }
            },
            {
                path: '/permission/create',
                name: 'permission.create',
                component: _import_page('permission/edit'),
                meta: {
                    auth: {
                        roles: {
                            permissions: ['write_permission']
                        },
                        redirect: {
                            name: 'login'
                        },
                        forbiddenRedirect: '/403'
                    }
                }
            },
        ]
    },
    {
        path: '/',
        component: require('@/AppBasic').default,
        children: [
            {
                path: '/register',
                name: 'register',
                component: _import_page('Register'),
                meta: {
                    auth: false
                }
            },
            {
                path: '/login',
                name: 'login',
                component: _import_page('Login'),
                meta: {
                    auth: false
                }
            },
            {
                path: '/404',
                name: 'error-404',
                component: _import_page('404'),
                meta: {
                    auth: undefined
                }
            },
            {
                path: '/403',
                name: 'error-403',
                component: _import_page('403'),
                meta: {
                    auth: undefined
                }
            },
        ]
    }
]
const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,
})
export default router